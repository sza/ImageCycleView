package com.weicong.library;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class ImageResAdapter extends PagerAdapter {

    /**
     * 图片视图缓存列表
     */
    private ArrayList<ImageView> mImageViewCacheList;

    /**
     * 图片资源id列表
     */
    private ArrayList<Integer> mImageRes;

    /**
     * item单击监听器
     */
    private ImageCycleView.OnItemClickListener mListener;

    /**
     * 上下文
     */
    private Context mContext;

    public ImageResAdapter(Context context, ArrayList<Integer> imageRes,
                           ImageCycleView.OnItemClickListener listener) {
        mContext = context;
        mImageRes = imageRes;
        mImageViewCacheList = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mImageRes.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        ImageView imageView;
        if (mImageViewCacheList.isEmpty()) {
            imageView = new ImageView(mContext);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else {
            imageView = mImageViewCacheList.remove(0);
        }
        imageView.setImageResource(mImageRes.get(position));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onItemClick(v, position);
                }
            }
        });
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ImageView view = (ImageView) object;
        container.removeView(view);
        mImageViewCacheList.add(view);
    }
}
