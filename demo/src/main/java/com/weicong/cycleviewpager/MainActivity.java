package com.weicong.cycleviewpager;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.weicong.library.ImageCycleView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ImageCycleView imageCycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageCycleView = (ImageCycleView) findViewById(R.id.imageCycle);

        ImageCycleView.Config config = new ImageCycleView.Config()
                .setIndicatorPos(ImageCycleView.RIGHT)
                .setIndicatorNormalRes(R.drawable.indicator_normal)
                .setIndicatorSelectRes(R.drawable.indicator_select)
                .setIndicatorMargin(5)
                .setBottomMargin(10)
                .setRightMargin(12)
                .setPlaceHolder(R.drawable.placeholder)
                .setTime(5000)
                .setCurrentIndex(2)
                .setOnItemClickListener(new ImageCycleView.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Toast.makeText(MainActivity.this, "position -> " + position, Toast.LENGTH_SHORT).show();
                    }
                });
        imageCycleView.setConfig(config);

        userImageRes();
//        userImageUrl();

        // 开始自动循环
        imageCycleView.start();
    }

    /**
     * 显示本地资源图片
     */
    private void userImageRes() {
        ArrayList<Integer> imageRes = new ArrayList<>();
        imageRes.add(R.drawable.image1);
        imageRes.add(R.drawable.image2);
        imageRes.add(R.drawable.image3);
        imageRes.add(R.drawable.image4);
        imageRes.add(R.drawable.image5);
        imageCycleView.setImageRes(imageRes);
    }

    /**
     * 显示网络图片
     */
    private void userImageUrl() {
        ArrayList<String> imageUrls = new ArrayList<>();
        imageUrls.add("http://img.my.csdn.net/uploads/201404/13/1397393290_5765.jpeg");
        imageUrls.add("http://img2.imgtn.bdimg.com/it/u=2696993843,2364483949&fm=21&gp=0.jpg");
        imageUrls.add("http://img4.imgtn.bdimg.com/it/u=4270507479,3433421651&fm=21&gp=0.jpg");
        imageCycleView.setImageUrl(imageUrls);
    }


    @Override
    protected void onStop() {
        imageCycleView.stop();
        super.onStop();
    }
}
